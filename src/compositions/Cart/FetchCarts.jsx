import Stack from "react-bootstrap/Stack";
import React, {useEffect, useState} from "react";
import axios from "axios";
import PropTypes from "prop-types";
import "./Cart.scss";
import "../../components/FetchFavorites/FetchFavorites.scss";
import ModalFavRemover from "./ModalFavRemover";


function
FetchCarts ({cart}) {
    const [product, setProduct] = useState(null);

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await axios.get(`https://fakestoreapi.com/products/${cart}`);
                const productData = response.data;
                setProduct(productData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [cart]);


    return (
        <div>
            {product ? (
                <Stack direction="horizontal" gap={3} className="horizontal-product">
                    <img src={product.image} className="p-2 cart-image" alt="cart image"/>
                    <div className="p-2 text-center">
                        <span className="cart-title">{product.title}</span>
                        <hr/>
                        {product.description}
                    </div>
                    <div className="p-2 ms-auto price-area">${product.price}</div>
                    <div className="vr" />
                    <div className="close-btn-container">
                        <ModalFavRemover  id={product.id}/>
                    </div>
                </Stack>
            ) : (
                <p>Loading...</p>
            )}
        </div>
    )
}
export default FetchCarts;

FetchCarts.propTypes = {
    cart: PropTypes.number
}