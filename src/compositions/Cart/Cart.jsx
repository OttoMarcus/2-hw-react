import React, {useEffect, useState} from 'react';
import "./Cart.scss";
import Stack from "react-bootstrap/Stack";
import FetchCarts from "./FetchCarts";
import Button from "react-bootstrap/Button";
import axios from "axios";
import {useSelector} from "react-redux";
import Modal from "react-bootstrap/Modal";
import LoginForm from "../../components/Authorization/LoginForm";
import ModalBlank from "../../components/Modal/ModalBlank";


function
Cart() {
    const [products, setProducts] = useState([]);
    const [subtotal, setSubtotal] = useState(0);
    const [isLoginForm, setIsLoginForm] = useState(false);
    const [show, setShow] = useState(false);
    const cartStore = useSelector(store => store.cart.cart);
    const fetchData = async () => {
        const productData = [];
        for (const car of cartStore) {
            try {
                const response = await axios.get(`https://fakestoreapi.com/products/${car}`);
                productData.push(response.data);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        setProducts(productData);
    }


    useEffect(() => {
        fetchData();
    }, [cartStore]);

    useEffect(() => {
        let totalSum = 0;
        for (const product of products) {
            totalSum += product.price;
        }
        setSubtotal(totalSum.toFixed(2));
    }, [products]);

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    const handleLoginForm = () => {
        handleClose();
        setTimeout(() => {
            setIsLoginForm(true);
        }, 500)
    }

    const handlePurchaseGuest = () => {
        window.location='/purchase';
    }

    return (
        <main className="main">
            <div className="container">
                <div className="title">Cart List</div>
                {cartStore.length > 0 ? (
                        <div className="cart-list">
                            <Stack gap={3}>
                                {cartStore.map((cart, index) =>
                                    <div className="p-2" key={index}>
                                        <FetchCarts
                                            key={cart.id}
                                            cart={cart}
                                        />
                                    </div>)}
                            </Stack>
                            <div className="sub-total">Subtotal: ${subtotal}</div>
                            <Button onClick={handleShow} variant="primary" className="checkout-btn">Go Checkout</Button>
                            {show && (
                                <Modal show={show} onHide={handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title className="text-center">Continue checkout</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body className="text-center">Log In or continue as guest?</Modal.Body>
                                    <Modal.Footer>
                                        <Button  variant="secondary" onClick={handleLoginForm}>
                                            Sign In
                                        </Button>
                                        <Button variant="primary" onClick={handlePurchaseGuest}>
                                            Guest
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                            )}
                        </div>) :
                    <div className="title">is Empty</div>}
            </div>
            {isLoginForm && <ModalBlank isModalOpen={isLoginForm}>
                <LoginForm/>
            </ModalBlank>}
        </main>
    )
}

export default Cart