import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import {useDispatch} from "react-redux";
import {removeCart_AC} from "../../store/cart.reducer";
import {addFavorite_AC} from "../../store/favorite.reducer";
// import {removeCart_AC} from "../../store/cart/cartSlice";
// import {addFavorite_AC} from "../../store/favorite/favoriteSlice";


function ModalFavRemover({id}) {
    const [show, setShow] = useState(false);

    const dispatch = useDispatch();
    const handleClose = () => setShow(false);

    const handleShow = () => setShow(true);

    const remover = () => {
        dispatch(removeCart_AC(id));
        handleClose();
    }

    const moveToFavorite = () => {
        dispatch(addFavorite_AC(id));
        dispatch(removeCart_AC(id));
         handleClose();
    }


    return (
        <>
            <div className="product-remover" onClick={handleShow}>
                <div className="close-favItem"></div>
            </div>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirm request</Modal.Title>
                </Modal.Header>
                <Modal.Body>Would you like add item to favorites?</Modal.Body>

                <Modal.Footer>
                    <Button variant="warning" onClick={moveToFavorite}>
                        Add to Favorite
                    </Button>
                    <Button variant="primary" onClick={remover}>
                        Remove
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default ModalFavRemover;