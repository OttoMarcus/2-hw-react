import React, {useState} from 'react';
import "./header.scss";
import NavDropdown from 'react-bootstrap/NavDropdown';
import { ReactComponent as Favorites } from "../../components/SVG/favorites.svg"
import { ReactComponent as Cart } from "../Cart/shopping-cart.svg";
import "../Cart/Cart.scss"
import checkLengthArr from "../../components/data/helpers";
import classNames from 'classnames';
import {Link, useLocation} from "react-router-dom";
import ModalBlank from "../../components/Modal/ModalBlank";
import LoginForm from "../../components/Authorization/LoginForm";
import {useSelector} from "react-redux";
import TogglePalitra from "../../components/contexts/AppContext/TogglePalitra";



function Header() {
    const  [logo, setLogo] = useState("/images/logo-passive.png");
    const [isSign, setIsSign] = useState(true);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const location = useLocation();
    const radioPath = ["", "mens_clothing", "women_clothing", "jewelery", "electronics"];
    const isRouteMatched = isCurrentRoute(location, radioPath);
    function isCurrentRoute(location, radioPath) {
        const currentPath = (location.pathname.substring(1));
        return !!radioPath.includes(currentPath);
    }

   function handleMouseOver() {
       setLogo("/images/logo-active.png");
    }

    function handleMouseOut() {
        setLogo('/images/logo-passive.png');
    }

    function toggleSign() {
       setIsSign(!isSign);
       if(!isSign) {
           setIsModalOpen(true);
       }
    }

    const loginBtn = classNames("btn",{
        "btn-lime": isSign,
        "btn-purple": !isSign
        })

    const cartStore = useSelector( store =>store.cart.cart);
    const favorite = useSelector(store => store.favorites.favorites);

    return (
        <nav className="navbar navbar-expand-lg bg-body-tertiary">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/"
                   onMouseOver={handleMouseOver}
                   onMouseOut={handleMouseOut}
                >
                    <img src={logo} alt="logo" width="80" className="logo-main" />
                </Link>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavDropdown title="Category" id="basic-nav-dropdown">
                                <NavDropdown.Item href="/mens_clothing">Men's Clothing</NavDropdown.Item>
                                <NavDropdown.Item href="/women_clothing">Women's Clothing</NavDropdown.Item>
                                <NavDropdown.Item href="/jewelery">Jewelery</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item href="/electronics">Electronics</NavDropdown.Item>
                            </NavDropdown>
                        </li>
                        <form className="d-flex" role="search">
                            <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
                            <button className="btn btn-lime" type="submit">Search</button>
                        </form>

                        {isRouteMatched && <TogglePalitra />}

                    </ul>
                    <Link to="/favorites" id="star-main">
                        <Favorites />
                        <div className="fav-summary">{checkLengthArr(favorite)}</div>
                    </Link>
                    <Link to="/cart" id="cart-main">
                        <Cart />
                        <div className="cart-summary">{checkLengthArr(cartStore)}</div>
                    </Link>
                    <button className={loginBtn} type="submit" onClick={toggleSign}>{isSign ? "Sign In" : "Sign Out"}</button>
                    {
                        isSign && <ModalBlank isModalOpen={isModalOpen}>
                            <LoginForm/>
                        </ModalBlank>
                    }
                </div>
            </div>
        </nav>
    )}

export default Header