import "./Footer.scss";

const currentYear = new Date().getFullYear();

function Footer() {
    return (
        <footer className="footer">
            <div className="text-center p-3">
                {currentYear}©<span className="copyright"></span> Copyright:
                <a className="text-white" href="https://dan-it.com.ua/">Dan-It-School & Co</a>
            </div>
        </footer>
    )
}

export default Footer;