import './App.css';
import PageContainer from "./components/PageContainer/PageContainer";
import Header from "./compositions/Header/Header";
import {Route, Routes} from "react-router-dom";
import Electronics from "./components/pages/Electronics";
import MenClothing from "./components/pages/MenClothing";
import Jewelery from "./components/pages/Jewelery";
import WomenClothing from "./components/pages/WomenClothing";
import PageNonFound from "./components/pages/PageNonFound";
import Footer from "./compositions/Footer/Footer";
import {useEffect} from "react";
import Favorites from "./components/pages/Favorites";
import Cart from "./compositions/Cart/Cart";
import NewUserBeginForm from "./components/Registration/NewUserBeginForm";
import { useSelector} from "react-redux";
import PurchaseNoRegistration from "./components/pages/PurchaseNoRegistration";
import ThankingPage from "./components/pages/ThankingPage";

function App() {
  const cartStore = useSelector((store)=>store.cart.cart);
  const favStore = useSelector((store)=>store.favorites.favorites);

  useEffect(() => {
    localStorage.setItem("car", JSON.stringify(cartStore));
  }, [cartStore])

  useEffect(() => {
  localStorage.setItem("fav", JSON.stringify(favStore))
  }, [favStore])



  return (
<>
    <Header />
    <Routes >
      <Route path="/" element={<PageContainer category={""}  />} />
      <Route path="/mens_clothing" element={<MenClothing />} />
      <Route path="/women_clothing" element={<WomenClothing/>} />
      <Route path="/jewelery" element={<Jewelery/>} />
      <Route path="/electronics" element={<Electronics />} />
      <Route path="/favorites" element={<Favorites />} />
      <Route path="/cart" element={<Cart/>} />
      <Route path="/new_user" element={<NewUserBeginForm/>} />
      <Route path="/purchase" element={<PurchaseNoRegistration/>} />
      <Route path="/thanking" element={<ThankingPage/>} />
      <Route path="*" element={<PageNonFound />} />
    </Routes>
    <Footer />
</>
  )}
export default App
