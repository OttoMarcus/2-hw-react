import {ErrorMessage, Field, Form, Formik} from "formik";
import "../Authorization/LoginForm.scss";
import React from "react";
import registerValidationSchema from "../Validation/registerValidationSchema";
import PrivacyPolicy from "./PrivacyPolicy";
import {Col, Container, Row} from "react-bootstrap";

const NewUserBeginForm = () => {

    const initialValues = {
        firstName: "",
        lastName: "",
        birthday: "",
        mail: "",
        password: "",
        passwordSubmit: "",
        isAccord: false
    }

    const handleSubmit = (values, {setSubmitting, resetForm}) => {

        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            resetForm(initialValues);
            setSubmitting(false);
        }, 2000);
    }

    const validationSchema = registerValidationSchema();

    return (
        <main className="main">
            <div className="page">
                <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={validationSchema}>
                    {({isSubmitting}) => (
                        <Container>
                            <Form>
                              <fieldset className="form-block">
                              <legend>New User</legend>
                                 <Row>
                                    <Col>
                                       <Field className="form-field" type="text" name="firstName" placeholder="First Name"/>
                                       <ErrorMessage name="firstName" component="div" className={"error err-validation"}/>
                                    </Col>
                                    <Col>
                                       <Field className="form-field" type="text" name="lastName" placeholder="Last Name"/>
                                       <ErrorMessage name="lastName" component="div" className={"error"}/>
                                    </Col>
                                 </Row>
                                 <Row>
                                    <Col>
                                       <Field className="form-field" type="date" name="birthday" placeholder="Date of Birth"/>
                                       <ErrorMessage name="birthday" component="div" className={"error"}/>
                                    </Col>
                                    <Col>
                                       <Field className="form-field" type="email" name="mail" placeholder="mail"/>
                                       <ErrorMessage name="mail" component="div" className={"error"}/>
                                    </Col>
                                 </Row>
                                 <Row>
                                    <Col>
                                       <Field className="form-field" type="password" name="password" placeholder="Password"/>
                                       <ErrorMessage name="password" component="div" className={"error"}/>
                                    </Col>
                                    <Col>
                                      <Field className="form-field" type="password" name="passwordSubmit" placeholder="Confirm password"/>
                                      <ErrorMessage name="passwordSubmit" component="div" className={"error"}/>
                                    </Col>
                                 </Row>
                                 <Row>
                                    <Col>
                                        <PrivacyPolicy name="isAccord"/>
                                    </Col>
                                 </Row>
                                    <hr/>
                                 <Row>
                                      <Col className="btn-center">
                                        <button className="btn btn-lime" type="submit" value="submit"
                                                disabled={isSubmitting}>Next steep ->
                                        </button>
                                      </Col>
                                 </Row>
                              </fieldset>
                            </Form>
                        </Container>
                    )}
                </Formik>
            </div>
        </main>
    )
}

export default NewUserBeginForm