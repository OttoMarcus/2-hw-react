import React, { useEffect } from "react";
import { Field, useFormikContext } from "formik";
import PropTypes from "prop-types";

const PrivacyPolicy = ({ name }) => {
    const { setFieldValue } = useFormikContext();

    useEffect(() => {
        setFieldValue(name, false); // Ініціалізація значення isAccord
    }, [name, setFieldValue]);

    const handlePrivacyPolicy = (e) => {
        const isAccord = e.target.checked;
         setFieldValue(name, isAccord);
    };

    return (
        <div className="form-check">
            <Field
                className="form-check-input"
                type="checkbox"
                id={name}
                name={name}
                onChange={handlePrivacyPolicy}
            />
            <label className="form-check-label" htmlFor={name}>
                Checking the box consent to collect and process personal data
            </label>
        </div>
    );
};

PrivacyPolicy.propTypes = {
    name: PropTypes.string.isRequired,
};

export default PrivacyPolicy;
