import {ErrorMessage, Field, useField} from "formik";
import {PatternFormat} from "react-number-format";
import "./PhoneField.scss";


function PhoneNumber({name, children}) {
    const [field] = useField({name,children});

    return (
        <div className="phone-field">
            <PatternFormat {...field} format="+1 (0##) ### ## ###" allowEmptyFormatting />

            <label htmlFor={name}>
                {children}
            </label>
            <ErrorMessage name={name} component="div" className={"error"}/>
        </div>
    )
}

export default  PhoneNumber