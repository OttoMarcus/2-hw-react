import {Card} from "react-bootstrap"
import Button from "react-bootstrap/Button";
import Image from 'react-bootstrap/Image';
import {useDispatch, useSelector} from "react-redux";

import ToggleFavorite from "../ToggleFavorite/ToggleFavorite";
import {addCart_AC} from "../../store/cart.reducer";
import {openModalAC} from "../../store/modal.reducer";
import {useEffect, useState} from "react";



function ProductsList({product}) {
const [inCart, setInCart] = useState("");
const defaultTitle = product.title.substring(0, 55);
const dispatch = useDispatch();
const ids = useSelector(store => store.cart.cart);

const getBtnStatus = () => {
    const values = Object.values(ids);
    if(values.includes(product.id)) {
        setInCart("Remove from Cart")
    }else {
        setInCart("Add to Cart")
    }
}

useEffect(() => {
    getBtnStatus();
},[])
const add2Cart = () => {
    getBtnStatus();
    dispatch(addCart_AC(product.id));
    dispatch(openModalAC(false));
}

    return (
            <Card  style={{ width: "55rem" }}>
                <Card.Header s="h5">
                    <div className="fav-container">
                    {defaultTitle}
                    <ToggleFavorite  product={product} />
                    </div>
                </Card.Header>

                <Card.Body className="productList-body">
                    <Image src={product.image} className="productList-image" />
                    <div className="productList-price-desc">
                        <Card.Title>Special price ${product.price}</Card.Title>
                        <Card.Text>{product.description}</Card.Text>
                    </div>
                </Card.Body>
                <div className="product-list-btn">
                    <Button variant="btn" className="btn-lime" size="sm" onClick={add2Cart} >{inCart}</Button>
                </div>
            </Card>
    )
}


export default ProductsList