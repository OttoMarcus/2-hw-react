import PropTypes from "prop-types";
import React, { useState} from "react";
import {useDispatch, useSelector} from "react-redux";

import ToggleFavorite from "../ToggleFavorite/ToggleFavorite";
import Modal from "../Modal/Modal";
import ProductFull from "./ProductFull";
import "../ToggleFavorite/ToggleFavorite.scss";
import "./productCard.scss";
import {openModalAC} from "../../store/modal.reducer";

function Product({product}) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const dispatch = useDispatch();
    // const IsModalOpen = useSelector(store => store.isModalOpen.openModal);
    const handleModalClose = () => {
        setIsModalOpen(false);
        dispatch(openModalAC(false));
        document.body.style.overflow = "auto";
    };

    const handleModalOpen = () => {
        setIsModalOpen(true);
        dispatch(openModalAC(true));
        document.body.style.overflow = "hidden"; //блокує background scroll
    };

    return (
        <div className="card col in-list" style={{ width: "17rem" }} >
            <div onClick={handleModalOpen}>
            <div className="hide-container">
                <img src={product.image} className="img-card" alt="product pict" />
                <h6 className="card-title">{product.title}</h6>
                <p className="price-container">
                    Price: $ <span className="price-regular">{product.price}</span>
                </p>
                <div className="favorite-on-product">
                  <ToggleFavorite product={product} />
                </div>
            </div>
            </div>
            {isModalOpen && (
                <Modal
                    modalClass="product-full"
                    title="Modal title"
                    closeButton={true}
                    onClose={handleModalClose}
                >
                    <ProductFull product={product} />
                </Modal>
            )}
        </div>
    );
}

Product.propTypes = {
    product: PropTypes.object,
}

export default Product;
