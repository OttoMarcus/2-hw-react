import React from "react";
import PropTypes from "prop-types";
import ModalCard from "../Modal/ModalCard";
import ToggleFavorite from "../ToggleFavorite/ToggleFavorite";
import {addCart_AC} from "../../store/cart.reducer";
import {useDispatch} from "react-redux";
import {openModalAC} from "../../store/modal.reducer";


function ProductFull({product}) {
const  dispatch = useDispatch();

    return (

        <div className="card col in-modal">
            <div className="hide-container">
                <div className="category bold">{product.category}</div>
                <div className="articleId">{`ART. ${product.id * 1286}`}</div>
                <img src={product.image} className="img-card" alt="product pict" />
                <h6 className="card-title">{product.title}</h6>
            </div>
            <ul className="list-group list-group-flush">
                <li className="list-group-item card-text">{product.description}</li>
                <li className="list-group-item ">Price: <span className="bold price">$ {product.price}</span></li>
                <li className="list-group-item">Rating: <span className="bold">{product.rating.rate} ({product.rating.count})</span></li>
                <li className="list-group-item product-full-bottom">
                    <ToggleFavorite product={product}/>
                    <ModalCard
                        title={"Confirm Action"}
                        body={"Do you want add the selected product to cart?"}
                        submit={"Confirm"}
                        click={() => {
                            dispatch(addCart_AC(product.id));
                            dispatch(openModalAC(false));
                            }
                        }
                        id={product.id}
                    />
                </li>
            </ul>
        </div>
    );
}

ProductFull.propTypes = {
    product: PropTypes.shape({
        image: PropTypes.string,
        title: PropTypes.string,
        id: PropTypes.number,
        description: PropTypes.string,
        category: PropTypes.string,
        price: PropTypes.number,
        rate: PropTypes.number,
        count: PropTypes.number
    })
};

export default ProductFull;
