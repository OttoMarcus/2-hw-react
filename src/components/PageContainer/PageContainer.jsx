import {useSelector} from "react-redux";
import Product from "../Products/Product";
import "../Products/productCard.scss";
import UseProducts from "../data/GetProducts";

import {useAppContext} from "../contexts/AppContext/AppContextProvider";
import ProductsList from "../Products/ProductsList";


function PageContainer({children, category}) {

    const [radioValue] = useAppContext();

    UseProducts();
    const loading = useSelector((store) => store.loading.loading);
    const products = useSelector((store) => store.products.products);
    let productSort = [];

    if (category) {
        productSort = products.filter(obj => obj.category === category);
    }else {
        productSort = products;
    }

    return (
        <div className="page-container">
            {children}
            {loading && (
                <div className="d-flex align-items-center">
                    <strong role="status" className="text-warning">Loading...</strong>
                    <div className="spinner-border text-warning ms-auto" aria-hidden="true"></div>
                </div>
            )}

            <div className="container text-center">

                <div className="row row-cols-auto">
                    {radioValue && productSort.map((product, index) =>
                        ( radioValue.name === "Icons" ?
                                <Product product={product} key={index} />
                                : <ProductsList product={product} key={index} />
                        ))
                    }
                </div>
            </div>
        </div>
    )

}
export default PageContainer
