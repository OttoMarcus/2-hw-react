import React from 'react';
import renderer from 'react-test-renderer';
import Alerts from './Alerts';

describe('Alerts component', () => {
    it('renders correctly when show is true', () => {
        const tree = renderer
            .create(
                <Alerts
                    headingText="Test Heading"
                    bodyText="Test Body"
                    show={true}
                    setShow={() => {}}
                    goMain={() => {}}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('renders correctly when show is false', () => {
        const tree = renderer
            .create(
                <Alerts
                    headingText="Test Heading"
                    bodyText="Test Body"
                    show={false}
                    setShow={() => {}}
                    goMain={() => {}}
                />
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
