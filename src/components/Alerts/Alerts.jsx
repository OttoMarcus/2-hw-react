
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import "./Alert.scss";

function Alerts({headingText, bodyText, show, setShow, goMain}) {

    const handleClick = () => {
        setShow(false);
        goMain();
    }

    return (
        <>
            <Alert show={show} variant="warning" className="alert-position">
                <Alert.Heading>{headingText}</Alert.Heading>
                <p>{bodyText}</p>
                <hr />
                <div className="d-flex justify-content-end">
                    <Button onClick={handleClick} variant="outline-success">
                        Close me
                    </Button>
                </div>
            </Alert>
        </>
    );
}

export default Alerts;