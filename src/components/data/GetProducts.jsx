import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {changeLoading} from "../../store/loading.reducer";
import {fetchProductsThunk} from "../../store/products.reducer";

//Ф-ція котра викорстовується для завантаження всіх продктів до стора.
 function UseProducts() {
     const dispatch = useDispatch();
    async function fetchProducts() {
       dispatch(changeLoading(true));
       await dispatch(fetchProductsThunk());
       dispatch(changeLoading(false));
    }

    useEffect(() => {
        fetchProducts();
    }, []);

    return {}
}

export default UseProducts