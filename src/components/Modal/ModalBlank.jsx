import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import {useEffect, useState} from "react";

function ModalBlank({ isModalOpen, children }) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);

    useEffect(() => {
        setShow(isModalOpen);
    }, [isModalOpen]);

    return (
        <>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title className="text-center">Sign In</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                        {children}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                        Enter
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default ModalBlank;