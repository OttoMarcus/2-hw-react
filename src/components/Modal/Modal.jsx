import './modal.scss';
import React from "react";
import PropTypes from "prop-types";

//  створюємо wrapper з кнопочкою закриття для модалки
function Modal({modalClass, onClose, closeButton, children}) {

    return (
        <div className="backdrop"  onClick={onClose} >
            <div className={modalClass} onClick={(e) => e.stopPropagation()}>
                {closeButton && <button className="close-button" onClick={onClose}>X</button>}
                {children}
            </div>
        </div>
    )
}

Modal.propTypes = {
    modalClass: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    closeButton: PropTypes.bool,
};

export default Modal