import {useEffect, useState} from 'react';
import "../../compositions/Header/header.scss";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import PropTypes from "prop-types";
import {useSelector} from "react-redux";

function ModalCard({title, body, click, submit, ...props}) {
  const [show, setShow] = useState(false);
  const [isInCart, setIsInCart] = useState();
  const inCart = useSelector(store => store.cart.cart);

    useEffect(() => {
        addCartBtn();
    }, []);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const addCartBtn = () => {
      const values = Object.values(inCart);
      if (values.includes(props.id)) {
          setIsInCart(true)
      } else {
          setIsInCart(false)
      }
  }
  const Confirm = () => {
      handleClose();
      click();
  }

  return (
    <>
      <Button variant="btn" className="btn-lime" onClick={() => {handleShow(); addCartBtn()}}>
          {isInCart? "Remove from Cart" : "Add To Cart"}
      </Button>

      <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} {...props} size="sm"
        aria-labelledby="contained-modal-title-vcenter"  centered >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            {body}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button onClick={Confirm} variant="btn" className="btn-lime">{submit}</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

ModalCard.propTypes = {
    title: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    submit: PropTypes.string.isRequired
}
export default ModalCard;