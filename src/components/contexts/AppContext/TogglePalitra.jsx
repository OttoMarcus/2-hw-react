import React, {useState} from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';
// import AppContext from "./AppContext";
import {useAppContext} from "./AppContextProvider";

function TogglePalitra() {
    const [radioValue, setRadioValue] = useAppContext();

    const radios = [
        { name: 'Icons', value: '1' },
        { name: "List", value: '2'}
    ];

    return (
        <>
            <ButtonGroup>
                {radios.map((radio, idx) => (
                    <ToggleButton
                        key={idx}
                        id={`radio-${idx}`}
                        type="radio"
                        variant={idx % 2 ? 'outline-secondary' : 'outline-info'}
                        name="radio"
                        value={radio.value}
                        checked={radioValue.value === radio.value}
                        onChange={() => {
                            setRadioValue(radio);
                        }}
                    >
                        {radio.name}
                    </ToggleButton>
                ))}
            </ButtonGroup>
        </>
    );
}

export default TogglePalitra;