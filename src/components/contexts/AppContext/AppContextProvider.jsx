import React, {useState} from 'react';
import AppContext from './AppContext';

export const AppContextProvider = ({ children, ...props }) => {
    const [radioValue, setRadioValue] = useState( { name: 'Icons', value: '1' });
    return <AppContext.Provider value={[radioValue, setRadioValue]}>{children}</AppContext.Provider>;
};

export function useAppContext() {
    const context = React.useContext(AppContext);
    if (!context) throw new Error('Use app context within provider!');
    return context;
}
