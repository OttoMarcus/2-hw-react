import {Container} from "react-bootstrap";
import "./Pages.scss";
import PropTypes from "prop-types";
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import {ReactComponent as Arrow} from "../SVG/arrow.svg";
import {ReactComponent as ArrowF} from "../SVG/arrowF.svg";
import {clearCart_AC} from "../../store/cart.reducer";
import {useState} from "react";
import Alerts from "../Alerts/Alerts";


function ThankingPage() {
    const dispatch = useDispatch();
    const {firstName, lastName, street, address2, city, region, zip, country} = useSelector(store => store.purchaseData.purchaseData);
    const navigate = useNavigate();
    const [show, setShow] = useState(false);

    const alertsMessage = "Just your order post we will provide the tracking number via email \n If you have any questions, feel free to contact us +1234567891";
    const headingText = "Congratulations!";
    const goBack = () => navigate(-1);
    const goMain = () => navigate("/");
    const redirect = () => {
        setShow(true);
        localStorage.setItem("car", JSON.stringify([]));
        dispatch(clearCart_AC());

        setTimeout(() => {
            goMain();
        }, 5000)

    }

    return (
        <main className="main">
            {show && <Alerts bodyText={alertsMessage} show={show} headingText={headingText} setShow={setShow} goMain={goMain} />}
            <div className="page">
                <Container>
                    <h2 className="title">thank you for your order</h2>
                    <h4 className="thank-msg">
                        <p>You just made our business grow, and</p>
                        <p>for that we are grateful!</p>
                    </h4>
                    <fieldset className="form-block checkout">
                        <legend className="title-sm">Shipping information</legend>
                        <ul className="column-static">
                            <li>Name:</li>
                            <li>Address:</li>
                            {address2 &&<li>Address:</li>}
                            <li>City:</li>
                            <li>Region:</li>
                            <li>ZIP: {zip}</li>
                            <li>Country:</li>
                        </ul>
                        <ul className="column-incoming-values">
                            <li> {firstName} {lastName}</li>
                            <li> {street}</li>
                            {address2 &&<li>{address2}</li>}
                            <li>{city}</li>
                            <li>{region}</li>
                            <li>{zip}</li>
                            <li>{country}</li>
                        </ul>
                    </fieldset>
                    <div className="arrow-container">
                        <div onClick={goBack} className="thank-msg arrow-txt"><Arrow alt="go back arrow"/> Return</div>
                        <div onClick={redirect} className="thank-msg arrow-txt go-foward">Confirm <ArrowF alt="go foward arrow"/></div>
                    </div>

                </Container>

            </div>
        </main>
    )
}

ThankingPage.propTypes = {
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    street: PropTypes.string,
}
export default ThankingPage