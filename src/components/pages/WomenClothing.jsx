import React from "react";
import PageContainer from "../PageContainer/PageContainer";

function WomenClothing() {
    return (
        <PageContainer category={"women's clothing" } />
    )
}

export default WomenClothing