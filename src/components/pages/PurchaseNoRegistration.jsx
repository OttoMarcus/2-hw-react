import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import {Container, Row, Col} from 'react-bootstrap';
import purchaseValidationSchema from "../Validation/purchaseValidationSchema";
import { useNavigate } from 'react-router-dom';
import PrivacyPolicy from "../Registration/PrivacyPolicy";
import "../Authorization/LoginForm.scss";
import {useDispatch} from "react-redux";
import {purchaseData_AC} from "../../store/purchaseDataReducer";
import PhoneNumber from "../Registration/PhoneNumber";



function PurchaseNoRegistration({history}) {
    const navigate = useNavigate();

    const initialValues= {
        firstName: "",
        lastName: "",
        mail: "",
        phone: "",
        street: "",
        address2: "",
        city: "",
        region: "",
        zip: "",
        country: ""
    }

    const goThanks = () => {
        navigate("/thanking")
    }

    const dispatch = useDispatch();
    const handleSubmit = (values, { setSubmitting, resetForm }) => {
        // dispatch(purchaseData_AC(JSON.stringify(values, null, 2)));
        const formData = { ...values };
        dispatch(purchaseData_AC(formData));
        setTimeout(() => {
            // resetForm(initialValues);
            setSubmitting(false);
            goThanks();
        }, 500);
    }

    const validationSchema = purchaseValidationSchema()

    return (
        <main className="main">
          <div className="page">
            <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={validationSchema}>
                {({isSubmitting}) => (
                    <Container>
                        <Form>
                            <fieldset className="form-block">
                                <legend>Complete you order</legend>
                                <Row>
                                    <Col>
                                        <Field className="form-field" type="text" name="firstName" placeholder="First Name"/>
                                        <ErrorMessage name="firstName" component="div" className={"error"}/>
                                    </Col>
                                    <Col>
                                        <Field className="form-field" type="text" name="lastName" placeholder="Last Name"/>
                                        <ErrorMessage name="lastName" component="div" className={"error"}/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Field className="form-field" type="mail" name="mail" placeholder="m@il"/>
                                        <ErrorMessage name="mail" component="div" className={"error"}/>
                                    </Col>
                                    <Col>
                                        <PhoneNumber name="phone" placeholder="Phone"/>
                                    </Col>
                                </Row>

                                <div className="title-small">Shipping Address</div>

                                <Row>
                                    <Col>
                                        <Field className="form-field" type="text" name="street" placeholder="Street name"/>
                                        <ErrorMessage name="street" component="div" className={"error"}/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Field className="form-field" type="text" name="address2" placeholder="Additional address"/>
                                        <ErrorMessage name="address2" component="div" className={"error"}/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Field className="form-field" type="text" name="city" placeholder="City"/>
                                        <ErrorMessage name="city" component="div" className={"error"}/>
                                    </Col>
                                    <Col>
                                        <Field className="form-field" type="text" name="region" placeholder="Region"/>
                                        <ErrorMessage name="region" component="div" className={"error"}/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Field className="form-field" type="number" name="zip" placeholder="Zip"/>
                                        <ErrorMessage name="zip" component="div" className={"error"}/>
                                    </Col>
                                    <Col>
                                        <Field className="form-field" type="text" name="country" placeholder="Country"/>
                                        <ErrorMessage name="country" component="div" className={"error"}/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <PrivacyPolicy name="isAccord" />
                                    </Col>
                                </Row>
                                <hr/>
                                <Row>
                                    <Col className="btn-center">
                                        <button className="btn btn-lime" type="submit" value="submit"
                                                disabled={isSubmitting}>Confirm
                                        </button>
                                    </Col>
                                </Row>
                            </fieldset>
                        </Form>
                    </Container>
                )}
            </Formik>
          </div>
        </main>
    )
}

export default PurchaseNoRegistration