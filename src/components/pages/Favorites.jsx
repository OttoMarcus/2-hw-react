import "./Pages.scss";
import Stack from 'react-bootstrap/Stack';
import FetchFavorites from "../FetchFavorites/FetchFavorites";
import "../FetchFavorites/FetchFavorites.scss"
import {useSelector} from "react-redux";


function Favorites () {
    const favStore = useSelector((store)=>store.favorites.favorites);

    return (
        <main className="main">
           <div className="title">Favorites</div>
           <div className="favorite-list">
               <Stack gap={3}>
                   {favStore.map((fav, index) =>
                       <div className="p-2" key={index}>
                           <FetchFavorites fav={fav} key={fav.id}  />
                       </div> )}
               </Stack>
           </div>
        </main>
    )
}


export default Favorites
