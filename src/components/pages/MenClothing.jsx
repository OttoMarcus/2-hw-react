import React from "react";
import PageContainer from "../PageContainer/PageContainer";

function MenClothing() {
    return (
        <PageContainer category={"men's clothing"}/>
    )
}

export default MenClothing