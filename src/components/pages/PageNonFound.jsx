import React from "react";
import "./Pages.scss";
import {Link} from "react-router-dom";


export default function PageNonFound() {
    return (
        <main className="main" style={{"background": "#fff", "textAlign":"center"}}>
            <div className="page_404">
                <div className="four_zero_four_bg">
                    <p className="four_zero_four">404</p>
                </div>
                <h1>Look like you're lost</h1>
                <h3>the page you are looking for not available</h3>
                <Link to="/" className="link_404">Go to Home</Link>
            </div>
        </main>
    )
}