
import React, {useEffect, useState} from "react";
import {ReactComponent as Star} from "../SVG/star.svg";
import "./ToggleFavorite.scss";
import {useDispatch, useSelector} from "react-redux";
import {addFavorite_AC} from "../../store/favorite.reducer";


function ToggleFavorite({product}) {
    const [isActive, setIsActive] = useState(false);
    const favStore = useSelector((store)=>store.favorites.favorites);
    const dispatch = useDispatch();

    useEffect(() => {
        const favIndex = favStore.indexOf(product.id)
        if (favIndex === -1) {

            setIsActive(false)

        } else {
            setIsActive(true)
        }
    }, [])

    const getFav = (event, id) => {
        event.stopPropagation();
        dispatch(addFavorite_AC(id))

        const favIndex = favStore.indexOf(id)

        if (favIndex !== -1) {
            setIsActive(false)
        } else {
            setIsActive(true)
        }
    }

    return (
        <Star onClick={(event) => getFav(event, product.id)}
              className={`${isActive ? 'active-favorite' : 'passive-favorite'}`}/>
    )
}

export default ToggleFavorite