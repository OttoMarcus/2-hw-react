import {ErrorMessage, Field, Form, Formik} from "formik";
import "./LoginForm.scss";
import {useState} from "react";
import loginValidationSchema from "../Validation/loginValidationSchema";
import {Link} from "react-router-dom";


const LoginForm = ({setShow}) => {
    const initialValues = {
        login: "",
        password: "",
    }

    const handleSubmit = (values, {setSubmitting, resetForm}) => {

        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            resetForm(initialValues);
            setSubmitting(false);
        }, 2000);
    }

    const [isHiddenPassword, setIsHiddenPassword] = useState(true);
    const [eyeOpen, setEyeOpen] = useState(true);
    const showPassword = (event) => {
        event.preventDefault();
        setIsHiddenPassword(!isHiddenPassword);
        setEyeOpen(!eyeOpen);
    }

    const validation = loginValidationSchema();

    const closeModal = () => {
        setShow(false)
    };

    return (
            <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={validation}>
                {({isSubmitting}) => (
                    <Form className="form">
                        <Field className="form-field login" type="email" name="login" placeholder="Login"/>
                        <ErrorMessage name="login" component="div" className={"error"}/>

                        <Field className="form-field login" type={isHiddenPassword ? "password" : "text"} name="password"
                               placeholder="password"/>
                        <span className="material-symbols-outlined eye"
                              onClick={showPassword}>{eyeOpen ? "visibility" : "visibility_off"}</span>
                        <ErrorMessage name="password" component="div" className={"error"}/>

                        <Link to="#" className="forget-link">Forgot password</Link>
                        <Link to="/new_user" className="sign-up-link" onClick={closeModal}>Sign up</Link>
                    </Form>
                )}
            </Formik>
    )
}


export default LoginForm