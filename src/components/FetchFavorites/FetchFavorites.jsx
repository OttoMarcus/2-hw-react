//каталог товарів у вкладці "улюблене"

import React, { useEffect, useState } from "react";
import axios from "axios";
import Stack from "react-bootstrap/Stack";
import "./FetchFavorites.scss";
import CategoryLink from "./CategoryLink";
import Button from "react-bootstrap/Button";
import PropTypes from "prop-types";
import {addCart_AC} from "../../store/cart.reducer";
import {useDispatch, useSelector} from "react-redux";
import {removeFavorite_AC} from "../../store/favorite.reducer";

function FetchFavorites({fav}) {
    const [product, setProduct] = useState();
    const dispatch = useDispatch();
    const cartStore = useSelector((store)=>store.cart.cart);
    let action = cartStore.includes(fav) ? "Remove from" : "Add to";

    useEffect(() => {         //витягую данні з активного fav
        async function fetchData() {
            try {
                const response = await axios.get(`https://fakestoreapi.com/products/${fav}`);
                const productData = response.data;
                setProduct(productData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [fav]);


    return (
        <div>
            {product ? (
                <Stack direction="horizontal" gap={3} className="favorite-item">
                    <img src={product.image} className="p-2 fav-image" alt="product image"/>
                    <CategoryLink category={product.category} />
                    <div className="p-2">{product.title}</div>
                    <div className="p-2 ms-auto">${product.price}</div>
                    <Button onClick={() => {dispatch(addCart_AC(product.id)) }} variant="btn" className="btn-lime">{action} Cart</Button>
                    <div className="vr" />
                    <div className="close-favItem" onClick={() => dispatch(removeFavorite_AC(product.id))}></div>
                </Stack>
            ) : (
                <p>Loading...</p>
            )}
        </div>
    );
}

export default FetchFavorites;

FetchFavorites.propTypes = {
    fav: PropTypes.number
}