import React from "react";

function CategoryLink({category}) {
    let value;

     switch (category) {
         case "men's clothing":
             value = "/mens_clothing"
             break;
         case "women's -clothing":
             value = "/women_clothing"
             break;
         case "jewelery":
             value = "/jewelery"
             break;
         case "electronics":
             value = "/electronics"
             break;
         default:
             value = "*"
     }
     return (
         <>
             <a href={value} className="p-2 categ">{category}</a>
         </>
     )
}
export default CategoryLink