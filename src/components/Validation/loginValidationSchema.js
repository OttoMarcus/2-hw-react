import * as Yup from 'yup';

const loginValidationSchema = () => {

    return Yup.object().shape({
        login: Yup.string().email("Invalid email").required("Required field"),
        password: Yup.string()
            .min(6, "Too Short")
            .max(50, "To Long")
            .required("Required")
    });
}

export default loginValidationSchema