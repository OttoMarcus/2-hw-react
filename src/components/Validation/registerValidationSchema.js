import * as Yup from 'yup';

const registerValidationSchema = () => {

    return Yup.object().shape({
        firstName: Yup.string()
            .min(3, "To Short")
            .max(20, "To long")
            .required("Field is Required"),
        lastName: Yup.string()
            .min(3, "To Short")
            .max(20, "To long")
            .required("Field is Required"),
        mail: Yup.string()
            .email("Invalid email").required("Required"),
        password: Yup.string()
                .min(6, "Too short!")
                .max(50, "Too long!")
                .required("Required"),
        passwordSubmit: Yup.string()
                // .min(6, "Too short!")
                // .max(50, "Too long!")
            .oneOf([Yup.ref("password"), null], "Password must match")
                .required("Required"),
        isAccord: Yup.boolean()
            .oneOf([true], "You must agree to continue")
            .required("You must agree to continue"),
        phone: Yup.string()
            .required("Required"),
    })
}

export default registerValidationSchema