import * as Yup from 'yup';

const purchaseValidationSchema = () => {

    return Yup.object().shape({
        firstName: Yup.string()
            .min(3, "Too Short")
            .max(50, "To Long")
            .required("Required"),
        lastName: Yup.string()
            .min(3, "Too Short")
            .max(50, "To Long")
            .required("Required"),
        mail: Yup.string().email("Invalid mail type").required("Required"),
        street: Yup.string()
            .min(5, "input street name")
            .required("Required"),
        city: Yup.string().required("Required"),
        region: Yup.string().required("Required"),
        zip: Yup.string().required("Required"),
        country: Yup.string().required("Required"),
        isAccord: Yup.bool().required()
    })
}

export default purchaseValidationSchema