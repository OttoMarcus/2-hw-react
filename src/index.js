import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./store/index.js";
import {AppContextProvider} from "./components/contexts/AppContext/AppContextProvider";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}>
        <BrowserRouter>
            <AppContextProvider>
               <App/>
            </AppContextProvider>
        </BrowserRouter>
    </Provider>
);

