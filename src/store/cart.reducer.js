const CART_ADD = "CART_ADD";
const CART_RM = "CART_RM";
const CART_CL = "CART_CL";

const initialState = {
    cart : JSON.parse(localStorage.getItem("car")) || []
}

export const addCart_AC = (id) => ({
    type: CART_ADD,
    payload: id
})

export const removeCart_AC = (id) => ({
    type: CART_RM,
    payload: id
})

export const clearCart_AC = (id) => ({
    type: CART_CL,
})

 const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case CART_ADD:
            const cartIndex = state.cart.indexOf(action.payload);
            let newCartList;
            if (cartIndex === -1) {
                newCartList = [...state.cart, action.payload];
            } else {
                newCartList = state.cart.filter((itemId) => itemId !==action.payload);
            }
            return {
                ...state,
                cart: newCartList
            }
        case CART_RM:
            return {
                ...state,
                cart: state.cart.filter((item) => item!== action.payload)
            }
        case CART_CL:
            return {
                ...state,
                cart: []
            }
        default:
            return state
    }
}

export default cartReducer
