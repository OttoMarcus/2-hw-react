import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    cart : JSON.parse(localStorage.getItem("car")) || []
}

export const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        addCart_AC: (state, action) => {
            const cartIndex = state.cart.indexOf(action.payload);
            let newCartList;
            if (cartIndex === -1) {
                newCartList = [...state.cart, action.payload];
            } else {
                newCartList = state.cart.filter((itemId) => itemId !==action.payload);
            }
            return {
                ...state,
                cart: newCartList
            }
        },
        removeCart_AC: (state, action) => {
            state.favorites = state.favorites.filter(favItem => favItem !== action.payload)
        }
    }
})

export const {removeCart_AC, addCart_AC} = cartSlice.actions;
export default cartSlice.reducer