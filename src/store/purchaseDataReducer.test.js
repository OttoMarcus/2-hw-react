import purchaseDataReducer, { purchaseData_AC } from './purchaseDataReducer.js';

describe('purchaseDataReducer', () => {
    it('should return the initial state', () => {
        expect(purchaseDataReducer(undefined, {})).toEqual({
            purchaseData: [],
        });
    });

    it('should handle PURCHASE_DATA action', () => {
        const initialState = {
            purchaseData: [],
        };
        const newData = [{ id: 1, product: 'Product 1' }];
        const action = purchaseData_AC(newData);
        const newState = purchaseDataReducer(initialState, action);
        expect(newState.purchaseData).toEqual(newData);
    });

    it('should return current state for unknown action', () => {
        const initialState = {
            purchaseData: [{ id: 1, product: 'Product 1' }],
        };
        const action = { type: 'UNKNOWN_ACTION' };
        const newState = purchaseDataReducer(initialState, action);
        expect(newState).toEqual(initialState);
    });
});
