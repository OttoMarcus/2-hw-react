const  ADD_FAV = "ADD_FAV";
const RM_FAV = "RM_FAV";

const initialState = {
    favorites: JSON.parse(localStorage.getItem("fav")) || []
}

export const  addFavorite_AC = (id) => ({
    type: ADD_FAV,
    payload: id
})

export const removeFavorite_AC = (id) => ({
    type: RM_FAV,
    payload: id
})

 const favoriteReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_FAV:
            const favIndex = state.favorites.indexOf(action.payload);
            let newFavList;
            if (favIndex === -1) {
                newFavList = [...state.favorites, action.payload];
            } else {
                newFavList = state.favorites.filter((itemId) => itemId !==action.payload);
            }
            return {
                ...state,
                favorites: newFavList
            }
        case RM_FAV:
            return {
                ...state,
                favorites: state.favorites.filter((item) => item!== action.payload)
            }
        default:
            return state
    }
}

export default favoriteReducer