import favoriteReducer, { addFavorite_AC, removeFavorite_AC } from './favorite.reducer.js';

describe('favoriteReducer', () => {
    it('should return the initial state', () => {
        expect(favoriteReducer(undefined, {})).toEqual({
            favorites: [],
        });
    });

    it('should handle ADD_FAV action', () => {
        const initialState = {
            favorites: [1, 2, 3],
        };
        const action = addFavorite_AC(4);
        const newState = favoriteReducer(initialState, action);
        expect(newState.favorites).toEqual([1, 2, 3, 4]);

        const action2 = addFavorite_AC(2);
        const newState2 = favoriteReducer(initialState, action2);
        expect(newState2.favorites).toEqual([1, 3]);
    });

    it('should handle RM_FAV action', () => {
        const initialState = {
            favorites: [1, 2, 3],
        };
        const action = removeFavorite_AC(2);
        const newState = favoriteReducer(initialState, action);
        expect(newState.favorites).toEqual([1, 3]);

        const action2 = removeFavorite_AC(4);
        const newState2 = favoriteReducer(initialState, action2);
        expect(newState2.favorites).toEqual([1, 2, 3]);
    });

    it('should return current state for unknown action', () => {
        const initialState = {
            favorites: [1, 2, 3],
        };
        const action = { type: 'UNKNOWN_ACTION' };
        const newState = favoriteReducer(initialState, action);
        expect(newState).toEqual(initialState);
    });
});
