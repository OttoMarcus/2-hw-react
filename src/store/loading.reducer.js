

const CHANGE_LOADING = "CHANGE_LOADING";

const  initialState = {
    loading: false
}
const loadingReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_LOADING:
            return {
                ...state,
                loading: action.payload
            }
        default:
            return state
    }

}

export const changeLoading = (isAction) => ({
    type: CHANGE_LOADING,
    payload: isAction
})

export default  loadingReducer;