import loadingReducer, { changeLoading } from './loading.reducer.js';

describe('loadingReducer', () => {
    it('should return the initial state', () => {
        expect(loadingReducer(undefined, {})).toEqual({
            loading: false,
        });
    });

    it('should handle CHANGE_LOADING action when true is passed', () => {
        const initialState = {
            loading: false,
        };
        const action = changeLoading(true);
        const newState = loadingReducer(initialState, action);
        expect(newState.loading).toEqual(true);
    });

    it('should handle CHANGE_LOADING action when false is passed', () => {
        const initialState = {
            loading: true,
        };
        const action = changeLoading(false);
        const newState = loadingReducer(initialState, action);
        expect(newState.loading).toEqual(false);
    });

    it('should return current state for unknown action', () => {
        const initialState = {
            loading: true,
        };
        const action = { type: 'UNKNOWN_ACTION' };
        const newState = loadingReducer(initialState, action);
        expect(newState).toEqual(initialState);
    });
});
