const PRODUCTS_REDUCER = "PRODUCTS_REDUCER";

const  initialState = {
    products: []
}

 const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCTS_REDUCER:
            return{
                ...state,
                products: [...state.products, ...action.payload]
            }
        default:
            return state
    }
}

export function fetchProductsThunk(){
    return async (dispatch) => {
        try {
            let response = await fetch("https://fakestoreapi.com/products/");
            let products = await response.json();
            dispatch(fetchProductsAC(products));
        } catch (error) {
            console.error("fetch products loading error", error)
        }
    }
}

export const fetchProductsAC = (products) => ({type: PRODUCTS_REDUCER, payload: products})
export default productsReducer