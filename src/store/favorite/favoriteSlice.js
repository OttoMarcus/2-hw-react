import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    favorites: JSON.parse(localStorage.getItem("fav")) || []
};

export const favoriteSlice = createSlice({
    name: "favorites",
    initialState,
    reducers: {
        addFavorite_AC: (state, action) => {
            if (!state.favorites.includes(action.payload)) {
                state.favorites.push(action.payload);
                // localStorage.setItem("fav", JSON.stringify(state.favorites));
            }
        },
        removeFavorite_AC: (state, action) => {
            state.favorites = state.favorites.filter(
                (item) => item !== action.payload
            );
            // localStorage.setItem("fav", JSON.stringify(state.favorites));
        },
        toggleFavorite_AC: (state, action) => {
            const index = state.favorites.indexOf(action.payload);
            if (index !== -1) {
                state.favorites.splice(index, 1);
            } else {
                state.favorites.push(action.payload);
            }
            // localStorage.setItem("fav", JSON.stringify(state.favorites));
        }
    }
});

export const { addFavorite_AC, removeFavorite_AC, toggleFavorite_AC } = favoriteSlice.actions;
export default favoriteSlice.reducer;
