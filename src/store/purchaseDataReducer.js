const PURCHASE_DATA = "PURCHASE_DATA";

const initialState = {
    purchaseData: []
}

export const purchaseData_AC = (purchaseData) => ({
    type: PURCHASE_DATA,
    payload: purchaseData
})

const purchaseDataReducer = (state = initialState, action) => {
    switch (action.type) {
        case PURCHASE_DATA:
            return{
                ...state,
                purchaseData: action.payload
            }
        default:
            return state
    }
}



export  default purchaseDataReducer