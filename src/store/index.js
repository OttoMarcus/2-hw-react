import {applyMiddleware, combineReducers, createStore} from "redux";
import {composeWithDevTools} from "@redux-devtools/extension";
import loadingReducer from "./loading.reducer";
import thunk from "redux-thunk";
import productsReducer from "./products.reducer";
import modalReducer from "./modal.reducer";
import cartReducer from "./cart.reducer";
import favoriteReducer from "./favorite.reducer";
import purchaseDataReducer from "./purchaseDataReducer";


const rootReducer = combineReducers({
    loading: loadingReducer,
    products: productsReducer,
    isModalOpen: modalReducer,
    cart: cartReducer,
    favorites: favoriteReducer,
    purchaseData: purchaseDataReducer,
})

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)) );

export default store;