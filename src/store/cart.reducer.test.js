import cartReducer, { addCart_AC, removeCart_AC, clearCart_AC } from './cart.reducer.js';

describe('cartReducer', () => {
    it('should return the initial state', () => {
        expect(cartReducer(undefined, {})).toEqual({
            cart: [],
        });
    });

    it('should handle CART_ADD action', () => {
        const initialState = {
            cart: [1, 2, 3],
        };
        const action = addCart_AC(4);
        const newState = cartReducer(initialState, action);
        expect(newState.cart).toEqual([1, 2, 3, 4]);

        const action2 = addCart_AC(2);
        const newState2 = cartReducer(initialState, action2);
        expect(newState2.cart).toEqual([1, 3]); // 2 is removed
    });

    it('should handle CART_RM action', () => {
        const initialState = {
            cart: [1, 2, 3],
        };
        const action = removeCart_AC(2);
        const newState = cartReducer(initialState, action);
        expect(newState.cart).toEqual([1, 3]);

        const action2 = removeCart_AC(4);
        const newState2 = cartReducer(initialState, action2);
        expect(newState2.cart).toEqual([1, 2, 3]); // No change as 4 is not in the cart
    });

    it('should handle CART_CL action', () => {
        const initialState = {
            cart: [1, 2, 3],
        };
        const action = clearCart_AC();
        const newState = cartReducer(initialState, action);
        expect(newState.cart).toEqual([]);
    });

    it('should return current state for unknown action', () => {
        const initialState = {
            cart: [1, 2, 3],
        };
        const action = { type: 'UNKNOWN_ACTION' };
        const newState = cartReducer(initialState, action);
        expect(newState).toEqual(initialState);
    });
});
