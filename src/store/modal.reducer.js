const OPEN_MODAL = "OPEN_MODAL";

const initialState = {
    openModal: false
}

export const openModalAC = (isOpenModal) => ({
    type: OPEN_MODAL,
    payload: isOpenModal
})
const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case OPEN_MODAL:
            return {
                ...state,
                openModal: action.payload
            }
        default:
            return state
    }

}

export default modalReducer