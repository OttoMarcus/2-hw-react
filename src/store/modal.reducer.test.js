import modalReducer, { openModalAC } from './modal.reducer.js';

describe('modalReducer', () => {
    it('should return the initial state', () => {
        expect(modalReducer(undefined, {})).toEqual({
            openModal: false,
        });
    });

    it('should handle OPEN_MODAL action when true is passed', () => {
        const initialState = {
            openModal: false,
        };
        const action = openModalAC(true);
        const newState = modalReducer(initialState, action);
        expect(newState.openModal).toEqual(true);
    });

    it('should handle OPEN_MODAL action when false is passed', () => {
        const initialState = {
            openModal: true,
        };
        const action = openModalAC(false);
        const newState = modalReducer(initialState, action);
        expect(newState.openModal).toEqual(false);
    });

    it('should return current state for unknown action', () => {
        const initialState = {
            openModal: true,
        };
        const action = { type: 'UNKNOWN_ACTION' };
        const newState = modalReducer(initialState, action);
        expect(newState).toEqual(initialState);
    });
});
